<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020-11-04
 * Time: 16:00
 */
namespace Aex\Packagetest\Facades;

use Illuminate\Support\Facades\Facade;

class Packagetest extends Facade

{
    protected static function getFacadeAccessor()

    {
        return 'packagetest';

    }

}